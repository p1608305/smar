# SMAR - AGAR.IO

## Project Description

Agar.io game clone made using reactive agents. The game contains 4 different types of entities :
- The reactive autonomous agents (red circles) : their goal is to eat "creeps" and other agents that are smaller to
become bigger, they also avoid bigger agents and obstacles.
- The "creeps" (green circles) : they move randomly around the screen and are eaten by agents to make them bigger.
- The obstacles (blue circles) : they are static and can't be eaten by agents but must be avoided.
- The avatar (yellow circle) : it is controlled by the player and can eat creeps and agents that are smaller than it to
grow. It is controlled by following the target (white circle) which is mapped to the mouse's position.

## Prerequisites
- Python 3.X
- Use `pip install -r requirements.txt` to install all dependencies

## How to run
Run `agario/main.py` to start the game.

## Controls
- R : restart the game
- Q : quit the game
- P : spawn autonomous agents
- C : spawn creeps
- O : spawn obstacles
- A : spawn avatar
- Control the avatar by moving the mouse cursor around the screen

## Known bugs/missing features
- Bug : the autonomous agents don't completely avoid obstacles 100% of the time
- Missing feature : the avatar doesn't avoid obstacles and can go over them.