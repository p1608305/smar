import random

from pygame.math import Vector2

import core


class Obstacle:
    def __init__(self):
        self.pos = Vector2(random.randint(0, 800), random.randint(0, 600))

        self.couleur = (0, 0, 255)
        self.taille = 5

    def draw(self):
        core.Draw.circle(self.couleur, self.pos, self.taille)
