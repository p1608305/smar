import random

from pygame.math import Vector2

import core


class Creep:
    def __init__(self):
        self.pos = Vector2(random.randint(0, 800), random.randint(0, 600))
        self.vel = Vector2(0, 0)
        self.acc = Vector2(0, 0)
        self.vivant = True

        self.couleur = (0, 255, 0)
        self.taille = 5

        self.maxSpeed = 5
        self.maxAcc = 1

    def draw(self):
        if self.vivant:
            core.Draw.circle(self.couleur, self.pos, self.taille)
        else:
            # core.Draw.circle((0, 0, 255), self.pos, self.taille)
            pass

    def move(self):
        if self.vivant:
            self.acc = Vector2(random.uniform(-1, 1), random.uniform(-1, 1))

            if self.acc.length() > self.maxAcc:
                self.acc.scale_to_length(self.maxAcc)

            self.vel = self.vel + self.acc

            if self.vel.length() > self.maxSpeed:
                self.vel.scale_to_length(self.maxSpeed)

            self.pos = self.pos + self.vel

            self.acc = Vector2(0, 0)

    def bordure(self, fenetre):
        if self.pos.y < 0:
            self.pos.y = fenetre[1]

        if self.pos.y > fenetre[1]:
            self.pos.y = 0

        if self.pos.x < 0:
            self.pos.x = fenetre[0]

        if self.pos.x > fenetre[0]:
            self.pos.x = 0
