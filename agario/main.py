import random

import pygame
import core
from agario.agar import Agar
from agario.avatar import Avatar
from agario.creep import Creep
from agario.obstacle import Obstacle
from agario.target import Target


def addAgent():
    core.memory("agars").append(Agar())


def supprAgent(agar):
    core.memory("agar").remove(agar)


def getAgent():
    pass


def computePerception():
    objTab = core.memory("creep") + core.memory("agars") + core.memory("obstacles") + core.memory("avatar")
    objDansVision = dict()
    for a in core.memory("agars"):
        perception = []
        for o in objTab:
            if a.circularFustrum.inside(o):
                if isinstance(o, Creep) and o.vivant:
                    perception.append(o)
                elif isinstance(o, Agar) and o.body.vivant:
                    perception.append(o)
                elif isinstance(o, Obstacle):
                    perception.append(o)
        objDansVision.update({a: perception})

    return objDansVision


def computeDecision():
    distanceCible = 10000
    distanceObstacle = 50
    objDansVision = computePerception()
    decisions = dict()

    for a in core.memory("agars"):
        cible = None
        obstacle = None
        for o in objDansVision[a]:
            if isinstance(o, Creep):
                if o.pos.distance_to(a.body.pos) < distanceCible:
                    cible = o
                    distanceCible = o.pos.distance_to(a.body.pos)
            elif isinstance(o, Agar) and o.body.taille < a.body.taille:
                if o.body.pos.distance_to(a.body.pos) < distanceCible:
                    cible = o
                    distanceCible = o.body.pos.distance_to(a.body.pos)
            elif isinstance(o, Agar) and o.body.taille > a.body.taille:
                if o.body.pos.distance_to(a.body.pos) < distanceObstacle:
                    obstacle = o
                    distanceObstacle = o.body.pos.distance_to(a.body.pos)
            elif isinstance(o, Obstacle):
                if o.pos.distance_to(a.body.pos) < distanceObstacle:
                    obstacle = o
                    distanceObstacle = o.pos.distance_to(a.body.pos)
        decisions.update({a: (cible, obstacle)})

    return decisions


def applyDecision(decisions):
    for a in core.memory("agars"):
        cible, obstacle = decisions[a]
        if obstacle is None:
            if cible is not None:
                if isinstance(cible, Creep):
                    a.body.attraction(cible.pos)
                elif isinstance(cible, Agar):
                    a.body.attraction(cible.body.pos)
            else:
                a.body.acc = pygame.Vector2(random.uniform(-1, 1), random.uniform(-1, 1))
        else:
            if isinstance(obstacle, Obstacle):
                a.body.repulsion(obstacle.pos)
            elif isinstance(obstacle, Agar):
                a.body.repulsion(obstacle.body.pos)

        if a.body.acc.length() > a.body.maxAcc:
            a.body.acc.scale_to_length(a.body.maxAcc)

        a.body.vel = a.body.vel + a.body.acc

        if a.body.vel.length() > a.body.maxSpeed:
            a.body.vel.scale_to_length(a.body.maxSpeed)

        a.body.pos = a.body.pos + a.body.vel

        a.body.acc = pygame.Vector2(0, 0)


def avatarTarget(avatar, target):
    avatar.body.attraction(target.pos)

    if avatar.body.acc.length() > avatar.body.maxAcc:
        avatar.body.acc.scale_to_length(avatar.body.maxAcc)

    avatar.body.vel = avatar.body.vel + avatar.body.acc

    if avatar.body.vel.length() > avatar.body.maxSpeed:
        avatar.body.vel.scale_to_length(avatar.body.maxSpeed)

    avatar.body.pos = avatar.body.pos + avatar.body.vel

    avatar.body.acc = pygame.Vector2(0, 0)


def draw():
    for a in core.memory("agars"):
        a.body.draw()
    for c in core.memory("creep"):
        c.draw()
    for o in core.memory("obstacles"):
        o.draw()
    for a in core.memory("avatar"):
        a.body.draw()
    core.memory("target").draw()


def initGame():
    core.memory("agars", [])
    core.memory("creep", [])
    core.memory("obstacles", [])
    core.memory("avatar", [])
    core.memory("target", Target())

    for i in range(0, core.memory("nbAgar")):
        core.memory("agars").append(Agar())
    for i in range(0, core.memory("nbCreep")):
        core.memory("creep").append(Creep())
    for o in range(0, core.memory("nbObstacles")):
        core.memory("obstacles").append(Obstacle())
    for a in range(0, core.memory("nbAvatar")):
        core.memory("avatar").append(Avatar())

    for a in core.memory("avatar"):
        a.body.couleur = (255, 255, 0)
        a.body.pos = pygame.Vector2(400, 300)


def setup():
    core.fps = 30
    core.WINDOW_SIZE = [800, 600]

    core.memory("nbAgar", 10)
    core.memory("nbCreep", 20)
    core.memory("nbObstacles", 5)
    core.memory("nbAvatar", 1)

    initGame()


def run():
    core.cleanScreen()

    # CONTROL
    if core.getKeyPressList("q"):
        pygame.quit()
    if core.getKeyPressList("r"):
        initGame()
    if core.getKeyPressList("p"):
        core.memory("agars").append(Agar())
    if core.getKeyPressList("c"):
        core.memory("creep").append(Creep())
    if core.getKeyPressList("o"):
        core.memory("obstacles").append(Obstacle())
    if core.getKeyPressList("a"):
        core.memory("avatar").append(Avatar())

    # SUPPRESSIONS DES AGENTS MORTS
    for a in core.memory("agars"):
        if not a.vivant:
            core.memory("agars").remove(a)
    for a in core.memory("avatar"):
        if not a.vivant:
            core.memory("avatar").remove(a)

    # AFFICHAGE
    draw()

    # MISE À JOUR DES POSITIONS
    decisions = computeDecision()
    applyDecision(decisions)
    for a in core.memory("agars"):
        a.body.bordure(core.WINDOW_SIZE)
    for c in core.memory("creep"):
        c.move()
        c.bordure(core.WINDOW_SIZE)
    for a in core.memory("avatar"):
        avatarTarget(a, core.memory("target"))
        a.body.bordure(core.WINDOW_SIZE)
    core.memory("target").update()

    # MANGER CREEPS
    food = core.memory("creep") + core.memory("agars") + core.memory("avatar")
    for a in core.memory("agars"):
        a.body.manger(food)
    for a in core.memory("avatar"):
        a.body.manger(food)


core.main(setup, run)
