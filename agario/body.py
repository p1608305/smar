import random
from pygame import Vector2

import core
from agario.creep import Creep

colorFamily = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]


class Body:
    def __init__(self, parent):
        self.parent = parent
        self.pos = Vector2(random.randint(0, 800), random.randint(0, 600))
        self.vel = Vector2(random.uniform(-5, 5), random.uniform(-5, 5))
        self.acc = Vector2()
        self.maxAcc = 1
        self.maxSpeed = 6
        self.minSpeed = 1

        self.couleur = (255, 0, 0)
        self.taille = 10

    def repulsion(self, obstacle):
        obstacleVect = Vector2(obstacle[0], obstacle[1])
        self.acc = self.pos - obstacleVect/10

    def attraction(self, obstacle):
        obstacleVect = Vector2(obstacle[0], obstacle[1])
        self.acc = obstacleVect - self.pos

    def move(self, creeps):
        pass

    def bordure(self, fenetre):
        if self.pos.y < 0:
            self.pos.y = fenetre[1]

        if self.pos.y > fenetre[1]:
            self.pos.y = 0

        if self.pos.x < 0:
            self.pos.x = fenetre[0]

        if self.pos.x > fenetre[0]:
            self.pos.x = 0

    def draw(self):
        if self.parent.vivant:
            core.Draw.circle(self.couleur, self.pos, self.taille)

    def manger(self, cibles):
        from agario.agar import Agar
        for c in cibles:
            if isinstance(c, Creep):
                if (c.pos.distance_to(self.pos) < self.taille + c.taille) and c.vivant:
                    c.vivant = False
                    self.taille += c.taille / 2
                    self.maxSpeed -= 0.2
                    if self.maxSpeed - 0.2 < self.minSpeed:
                        self.maxSpeed = self.minSpeed

            elif isinstance(c, Agar) and c.body.taille < self.taille:
                if (c.body.pos.distance_to(self.pos) < self.taille + c.body.taille) and c.vivant:
                    c.vivant = False
                    self.taille += c.body.taille / 2
                    self.maxSpeed -= 0.2
                    if self.maxSpeed - 0.2 < self.minSpeed:
                        self.maxSpeed = self.minSpeed

    def update(self):
        pass
