from pygame import Vector2

from agario.creep import Creep
from agario.obstacle import Obstacle


class circularFustrum:
    def __init__(self, parent):
        self.radius = 360
        self.parent = parent

    def inside(self, obj):
        if hasattr(obj, "pos"):
            if isinstance(obj, Creep) or isinstance(obj, Obstacle):
                if isinstance(obj.pos, Vector2):
                    if obj.pos.distance_to(self.parent.body.pos) < self.radius:
                        return True
            elif isinstance(obj, self.parent.__class__):
                if isinstance(obj.body.pos, Vector2):
                    if obj.pos.distance_to(self.parent.body.pos) < self.radius:
                        return True
        return False
