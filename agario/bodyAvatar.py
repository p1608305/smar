from pygame import Vector2

from agario.agar import Agar
from agario.body import Body
from agario.creep import Creep


class BodyAvatar(Body):
    def __init__(self, parent):
        super().__init__(parent)
        self.couleur = (255, 255, 0)
        self.pos = Vector2(400, 300)
        self.distTarget = 20
        self.ressort = 10

    def attraction(self, target):
        targetVect = Vector2(target[0], target[1])
        forceRappel = self.ressort * (self.pos.distance_to(targetVect) - self.distTarget)
        self.acc = forceRappel * (targetVect - self.pos)
