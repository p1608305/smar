from agario.agar import Agar
from agario.bodyAvatar import BodyAvatar


class Avatar(Agar):
    def __init__(self):
        super().__init__()
        self.body = BodyAvatar(self)
