from pygame import Vector2

import core
from agario.circularFustrum import circularFustrum
from agario.body import Body


class Agar:
    def __init__(self):
        self.body = Body(self)
        self.circularFustrum = circularFustrum(self)
        self.perception = []
        self.vivant = True

    def eat(self, obj):
        for o in obj:
            if o.pos.distance_to(self.pos) < 10:
                o.freeze = True
                print("Eat")

    def filtre(self):
        centre = None
        voisins = []

        for p in self.perception:
            if isinstance(p, "object"):
                centre = p
            if isinstance(p, "Body"):
                voisins.append(p)

        return centre, voisins

    def update(self):
        pass
