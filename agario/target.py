import random

import pygame.mouse
from pygame.math import Vector2

import core


class Target:
    def __init__(self):
        self.pos = pygame.mouse.get_pos()

        self.couleur = (255, 255, 255)
        self.taille = 2

    def draw(self):
        core.Draw.circle(self.couleur, self.pos, self.taille)

    def update(self):
        self.pos = pygame.mouse.get_pos()
